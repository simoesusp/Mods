#) PATCH: Va´ ate´: https://www.pacificfury.net/pacific-storm-allies-1-8

#) DESTINO: "C:\Program Files (x86)\Steam\steamapps\common\Pacific Storm - Allies\bin\alliesnew.exe"

#) INICIAR EM: "C:\Program Files (x86)\Steam\steamapps\common\Pacific Storm - Allies\bin"

#) Nao comprimir os SAVEGAMES pois quando crachar, o PSA_Tool nao consegue abrir os Saves Comprimidos

#) Nao pagar pra Fazer Paz com a GERMANY, pois eles voltam a declarar guerra uns dias depois!!

#) NUNCA saia da BATALHA com os AVIOES Voando e Carries..., pois cracha o jogo!!! Retreat ou pousa nos Carriers (mesmo assim Cracha!! ==> Usar sempre Attak com Aircraft com os Carries longe da BAse, se botar os Carries na Batalha vai Crachar!!)

#) BATALHAS: Sair frequentemente para o Strategic Level e salvar o jogo e depois voltar pra batalha! (pode roubar e sair um pouco antes dos navios serem atingidos por torpedos!!)

#) SUBS INIMIGOS: Os P-51 com deaph charge matam os subs inimigos no AUTO_BATLLE

#) Os 600 P-51 com 10xRL127 Rockets destroem tudo nas bases (Avioes, 150 Cargueiros, Petroleiros, Destroiers) So´ nao matam SUBS nem Havy Cruisers pra cima!!

#) Depois de matar os Destroiers e Planes (Com RL-127 rockets) os 600 P-51 com 2x AP640 matam TODOS os 20 Havy Cruisers da Base.

#) Depois mandar os B-24-T com Soldiers pra tomar a base!!

#) CORES: Verde=UK; Rosa=Holland; Red=Russos; AmareloCaganera=Japas; Azul=USA; Roxo_Azulado=German

#) MENSAGENS: ligar somente Canetinha + Puse + !  ==> Para construction e Load/Unload
	- A Bolinha e´ a janelinha com OK que abre.
	- E´ o ! que abre a janela boa de ir ate´ o lugar que ocorreu a mensagem
	
#) PRODUCTION: 
	- Sempre produzir o nr. maximo de navios (10) ou planes (100) pois produz mais rapido
	- Sempre escolher o custo Minimo
	- Segurar o SHIFT pra fazer 5 copias da producao
	- Marcar na PRODUCTION com ALT-Click o ultimo dos navios ou avioes da lista de producao, para receber uma mensagem quando produzir!


#) F2   muda o nome das Formations

#) Ctrl-B   faz avisar quando chegar nas Bases

#) ALT-click   Mostra INFO window da Formation (no mapinha ou na Base)

#) Usar Espaco 2x para fechar Janelas com Botao de OK e despausar o jogo 

#) Cada ACADEMIA da´ 25 ENG por vez (a cada SEMANA)

#) Viajar entre as Bases durante o DIA pra evitar Craches na Aterrizagem!

#) Manter DIPLOMACIA com a RUSSIA em 65 pra nao declarar guerra contra mim quando Japan declarar Guerra (Mesmo no Historical Game!!)!!

#) Se tomar mais que 25 BASES e for mais ou igual que 08/1941, a GERMANY declara guerra contra a gente!!

#) Pode VENDER TECS pra o Japao ou Germany, pois eles Roubam e vao fazer MOD-3 Units de qq jeito!

#) TRANSPORT ENG: Usar PBY Transport no inicio do jogo pra transportar ENG pra varias bases (Eles pousam na AGUA -> mas perde varios avioes!)
	- B24-T -> Precisa de Airport Level 2 pra pousar. mais tem mais Range (melhor pro final do jogo!)
	- Podem levar RAdio e Amunition para as bases 

#) EQUIPAMENTOS das Modificacoes: 
	- Planes: SCR e´ o melhor!! AN/ARC-7 e´ melhor que ARC-3A
	- Gunsight - Type N-3A e´ melhor
	- Bomb sight - D-8 e´ melhor que T-1
	- Radar - Westinghouse ABS
	- Ships: TBS RAdio; Narrow Beam; 

#) INICIO DO JOGO:
	- Tomar o maior nr de BASES possivel (Ctrl-C)
	- Mudar a Capital pra Monterrey (clicar com o botao direito na base no Mapa)
	- Se passar de 25 Bases, os Germans declaram guerra em 08/1941!! (tem que ter P-38 pra se defender (precisa de Aeroporto level-1!!))
	- Trocar algumas Bases com os Netherlands ou Russos pra nao passar de 25 e conseguir dinheiro pra pesquisa e construcao
	- Nunca deixar acabar o Dinheiro (se acabar, dar prioridade pra pesquisa do Radar e P-51)
	- Nao pesquisar 20mm Aircraft gun; HF Radar e Sonar  ... Esperar pra comprar e revender pros ENGLAND e JAPAs
	- Nao pesquisar OBTAIN NUCLEAR WEAPONS -> Trocar depois com UK
		- Se ligar as MISSOES, cuidado, pois eles podem pedir pra pesquisar isso
	- Ligar as missoes pra conseguir um pouco de Moral e dinheiro no inicio do jogo... Depois vai ficar chato pra caramba!!
		- Pode cancelar as novas Missoes e continuar cumprindo as antigas!
	- Trancar a Producao e Fazer primeiro as fabriacas das 4 bases mais ricas pra ganhar mais dinheiro pras outras.
	- Investir tudo na Pesquisa pra nao faltar dinheiro e se acabar a grana, priorizar 1700HP e Laminar Foil
	- FABRICAR: 20xTransporte Victory, 50xSubs e 20xEvarts. -> E´ baratinho e faz rapido!
	- Deixar o Essex e 1 Iwoa fabricar, pois termina antes do inicio da guerra	
	- Quando Pesquisar 1700HP: Fazer bastante TBF, SBD2 e F6F pra equipar os Carriers
	- Quando Pesquisar o Laminar, Parar TUDO e so´ fazer P-51
	- Nao vale a pena fazer P-38 e B-24... Esperar pelos P-51
	- Depois que se proteger bem com uns 1000 P51, pode fazer uns 200 B-24  (demora bastante) pra usar pra atacar bases e Ships de longe!
	- Fabricar Ammo e botar Pilotos e Sailors num Transporte pra treinar quando Fizer Shooting Range em Monterrei
	- Fazer Fabricas nas Bases que valem a pena (primeiro as que dao dinheiro!! e depois nas que dao oleo) Vender as outras!
	- Fazer aeroporto Level-0 (para os P-51 passarem) nas Bases que fecham caminho ate´ os japas: Pearl-Harbor; Midway; Wake; Guam/Saipan
	- Fazer Airport Level-2 em Wake e Manila pra os B-24 poder passar (eles pulam direto de San Diego pra Wake!)
	- Fazer aeroporto Level-0 em TODAS AS BASES pra proteger com P-51 => os P-38 precisam de Level-1

#) PODUCAO BASES: Fazer Fabricas nessas que valem a pena (primeiro as que dao dinheiro!!) Vender as outras!
>  Base15 - Monterey - M- 57500, A- 390, I- 19500, O- 39000
>  Base16 - San Diego - M- 17250, A- 390, I- 13000, O- 39000
>  Base14 - Bremerton - M- 17250, A- 390, I- 13000, O- 39000
>  Base55 - Panama - M- 17250, A- 520, I- 19500, O- 26000

F> Base48 - Pearl Harbor - M- 2000 , A- 0 , I- 0 , O- 0
>  Base4-Dutch Harbor - M- 1050, A- 0, I- 0, O- 1100

F> Base57-Manila - M- 1600 , A- 60 , I- 600 , O- 0
>  Base58-Tacloban - M- 1000 , A- 50 , I- 500 , O- 2500
F> Base62-Davao - M- 1000 , A- 50 , I- 3000 , O- 0

>  Base71-Miri - M- 1600 , A- 60 , I- 1800 , O- 21000
>  Base87-Bandjermasin - M- 1200, A- 0, I- 0, O- 4000

>  Base75-Hollandia - M- 3450 , A- 0 , I- 3900 , O- 13000

>  Base69-Saigon - M- 3450 , A- 65 , I- 6500 , O- 19500
F> Base79-Bangkok - M- 1667 , A- 33 , I- 667 , O- 0
Base80-Kota Baru - M- 550 , A- 0 , I- 0 , O- 100
Base85-Palembang - M- 550, A- 0 , I- 100 , O- 2000
>  Base123-Batavia - M- 3450 , A- 0 , I- 1300 , O- 13000

F> Base36-Port Arthur - M- 11500 , A- 0 , I- 3900 , O- 0
>  Base37-Shanghai - M- 2299, A- 390, I- 2600, O- 1300
F> Base38-Okinawa - M- 1999 , A- 0 , I- 0 , O- 0
Base40-Kaochung - M- 600 , A- 0 , I- 600 , O- 0
Base59-Haikou - M- 931 , A- 0 , I- 1733 , O- 0

>  Base28 - Osaka - M- 8046 , A- 65 , I- 910 , O- 390
>  Base35 - Nagasaki - M- 8045 , A- 65 , I- 650 , O- 390
>  Base29 - Tokyo - M- 9168 , A- 65 , I- 650 , O- 390
>  Base19 - Nigata - M- 1691 , A- 0 , I- 0 , O- ?


#) ATACAR COM AVIOES: 
   - Usar ALT -> pra ajustar a Altitude e Velocidade: Level significa que todos andam na mesma velocidade (funciona pra Navios tambem!!)
   - B-24 ==> Usar para BOMB Ships 
	- usar altitude 470m e Speed 90% --> Eles nunca erram, e nao morrem com Anti-ar das bases. Mas as perdas sao altas com Destroyers.
	- Para atacar navios, usar 1200m e  Speed 90% --> Eles erram bastante, mas sobrevivem mais
	- Sempre usar 100 avioes. E NUNCA atacar com o restinho que sobrar...
	- Usar sempre 8x AP640
	- Range(Motor Mais Moderno): 5661Km/2 (8X640AP)
	- Aeroporto Level 2
	- Pode levar SOLDIERS a bordo pra tomar as BASES => Destruir as Art- Anti-Air e depois liberar no AUTOBATLE (DESLIGANDO o Destroy Infrastructure) pra tomar a Base
	==> Usar para Matar Planes --> Eles atiram pra todos os lados!!!
   - P-51 ==> Usar para DIVE-BOMB Ships (E´ muito rápido de Fazer!!)
	- usar 2000m de altitude e uns 80% da velocidade --> Eles vao se aproximar bem alto pra escabar da Anti-Air e depois baixam pra 1200m pra fazer o Dive-Bomber e voltam pra grande altitude!
	- Range: 1642Km/2 (2X150EFI+2X640AP)
	- Aeroporto Comum
	- Sempre usar 100 avioes. E NUNCA atacar com o restinho que sobrar...
	==> Fazer milhares (100 ou 50 de cada vez quando pesquisar) Da´ pra esperar pesquisar o Motor de 1700HP e Laminar Foil e entao fazer milhares
	==> Selecionar (4X200EFI) que eles andam 4516Km que da´ pra cruzar de Monterrey pra Pearl Harbor direto!!

#) COMO ATACAR BASES: 

	- METODO ALTERNATIVO (--> Nao esqecer de desligar o Destroy Base Structures!!!):
		Passo 0) Mandar P-51 com 10xRL127 Rockets pra destruir Aircraft Artilhary e talvez Cost Artilhary

		Passo 1) Os 600 P-51 com 10xRL127 Rockets destroem tudo nas bases (Avioes, 150 Cargueiros, Petroleiros, Destroiers) So´ nao matam SUBS nem Havy Cruisers pra cima!!

		Passo 2) Depois de matar os Destroiers e Planes (Com RL-127 rockets) os 600 P-51 com 2x AP640 mataram TODOS os 20 Havy Cruisers da Base.

		Passo 3) Depois mandar os B-24-T com Soldiers pra tomar a base!!

	--> DESTROIER Killer: P51 com 10XRocket127 -- Range de 2310Km no AUTOBATTLE => 2000 P51 x 150 Kagero+38 Mogami e Morreu somente 9 P51 e matou TODOS Kagero, mas nao matou NENHUM Mogami -> Os foguetes sao fracos e nao Matam os Mogami, nem suas torres principais!
	
	- METODO ANTIGO: Quando tem muitos Navios e avioes na Base... Atacar primeiro com P51 e destruir os Anti-Air e se der tempo, os Artilhary... Dai´ preparar um Segundo ataque com uns 4000 avioes, com P51 com Rockets + SBD + TBF + B24 ... E desligar o Damage Base Buildings... E deixar pra ver o que da´... Normalmente eles destroem TUDO!!! Depois da´ pra fazer um terceiro ataque com Transporters com SOLDADOS pra conqueistar a  Base!! Se ainda tiver muitos Soldados Japas, entrar com Navios e bombardear um pouco das casinhas... Pois vai matar miuto dos soldados e depois fazer um novo ataque com trnasporters pra conquistar a base...

	--> TAmbem da´ pra invadir manualmente com 10 YOWAS que eles destroem tudo se o inimigo nao tiver Battleships!!	

	- Matar todos Ships e depois as Artilharias e eventualmente os Avioes
	- Depois sair e Desligar o Destroy Base Infrastructure e ligar o AUTOBATTLE pra Conquistar a Base com os Soldados 
	- tem que ter tipo uns 3x mais soldados pra conquistar a base 
	- Os eng, Sailors e Pilots tambem Lutam!!
	- Aconteca o que acontecer, nao destrua os AEROPORTOS!! (Senao nao dara pra defender com P-51 !!)
	- Se tiver muitos SOLDADOS, usar bombers ou Iowas pra bombardear os Barracks pra mata-los antes de tentar tomar a base!
	- Levar Transports com bastante soldados e mantelos longe da base ate´ matar todo mundo!

	==> Se nao alcancar com os P-51 ou B-24, Chegar perto com os Carriers e usar Attack with Aircraft e atacar somente com os Avioes!!  (com Battleships pra proteger de eventuais ataques vindo da base com Navios!) (nao se preocupar com ataques de avioes, pois eles morrem todos!!)
		- Pode selecionar amunitions pros F6F e TBF pra usar AP640 e usar todo mundo pra bombardear os Navios!!
		- Na Formacao dos Carriers, Levar incluido Destroyers pra se proteger de atakes de submarinos e 1 Transporte e 1 Tanker
			- Levar 1 transporte com bastante municao e eventualmente Torpedos pra recarregar os avioes no mar
			- Levar 2 tankers pra recarregar o Fuel!

#) BATALHA: 
	- ZONES: Cuidado com o lado do Sol/Lua pra nao ofuscar os teus Avioes! 
	- Usar Zone 6 no Mapa padrao do Mar
	- Left-Ctrl+Middle mouse -> Group Rotate Units
	- Right-Ctrl+Middle mouse -> Individually Rotate Units
	- Backspace turn off ICONS
	- SUB DIVE DEPTH O que mostra no Botao e´ a PROXIMA depth
	- Ctrl-Z -> Select Units of the same type da Selecionada
	- Z -> Select Units of the same type da Selecionada ON SCREAN
	- A -> Select Units ON SCREAN
	- CTRL+FRAME -> Attack todos dentro do Frame
	- Ctrl+W -> Pra RETREAT !!


#) CONTROLAR PLANES na BATALHA: 
	- A -> Autopilot
	- Z/X -> Pedals
	- PG-Up/Down -> Speed
	- D/C -> Flaps
	- SPACE -> Fire ; Ctrl-SPACE -> Fire Bomb
	- B -> Open Bomb Bay
	- Enter -> Select Target
	- Shift -> Look at the target
	- Ctrl-TAB -> Switch Planes
	- U -> Take control de dentro
	
#) CONTROLAR SHIPS na BATALHA: 
	- Shift+Right Click -> Attack sem mudar movimento

	==> BATTLESHIPS na BATALHA: 
		- Usar Iowas ou Colorados Alinhados e andando na DIAGONAL pra fugir dos Torpedos! 
		- Se os Destroyers Japas chegarem ao alcance dos canhoes pequenos deles (Circulos Azulzinhos), vao lancar os TORPEDOS!! 

	==> Nunca entrar com os Carriers na Batalha, pois SEMPRE da´ Crash to Descktop!!!
		- Se entrar so´ pra se divertir e depois APAGAR as formations que matou na base no SaveGame: 
			- Fugir sempre pro mais longe possivel dos Destroyers Japas
			- Ficar sempre longe do alcance dos canhoes dos cruisers e Battleships
			- AutoLaunch: botar os planes em (Nao agressive) pra eles ficarem rodeando sobre os CVs ate´ todo mundo decolar
			- Atacar sempre com todos JUNTOS
			- Quando volta pra pegar mais bombas, os planes se "concertam"!!!
			- Manter uns 10 F6F em CAP pra proteger os CAriers dos Bombers inimigos

#) AVIOES e AEROPORTOS:  ==> Quando tu abre o menu pra escolher as bombas dos avioes, o Range e´ calculado e aparece escrito ali!!!
	
	- B17 -> LEVEL 2 UPGRADE - Range(Motor Original): 	6520Km(4X640AP), 8506Km(8X200EFI)
				 - Range(Motor Mais Moderno): 	6474Km(4X640AP), 8000Km(8X200EFI)
				 - deveria ser 3219km com 6000lib bomb, 6040 ferry range 
	- B24 -> LEVEL 2 UPGRADE - Range(Motor Original): 	3146Km(8X640AP), 5203Km(8X200EFI)
				 - Range(Motor Mais Moderno): 	5661Km(8X640AP), 9528Km(8X200EFI)
	- PBY -> Agua		 - Range: 			4626Km(2XMK13-Torp), 4330Km(4XDC125)
	- PBY Transport -> Agua	 - Range: 			5976Km(4XEFI)
	- P38 -> LEVEL 1 UPGRADE - Range(MotOrig):1241Km(2X150EFI+2X640AP), 2138Km(2X150EFI), 2636Km(4X200EFI) 
				 - Range(1700hp): 981Km(2X150EFI+2X640AP),  2336Km(4X200EFI)
	- P51 -> Airport Comum	 - Range: 	  1642Km(2X150EFI+2X640AP), 4200Km(2X150EFI), 4516Km(4X200EFI), 2310Km(2XRocket127), 2310Km(10XRocket127)
				 - deveria ser 3700km com drop tanks
	- F6F -> Airport Comum	 - Range: 	  2905Km(1X150EFI), 1280Km(1X150EFI+6RL127+2X500HE), 1290Km(1X150EFI+2XAP640) 
				 - deveria ser 1500km combat, 2460 ferry range
	- 2B2C-> Airport Comum	 - Range: 	  3155Km(2X75EFT+1XAP640), 3003Km(2X75EFT+2XAP400) 
				 - deveria ser 1800km com 1000lib bomb
	- TBF -> Airport Comum	 - Range: 	  1390Km(1XAP640), 860Km(1XMK13-Torp), 2424km(1x150EFI) 
				 - deveria ser 1600km com 1 Torp
	- F8F -> Airport Comum	 - Range: 	  3830Km(1X150EFT+2X75EFT), 2800Km(1X150EFT), 2790Km(1X150EFI+2X640AP), 1700Km(2XRocket127)
						  2825Km(1X150EFT+6XRockets) 

	- F4U -> Airport Comum	 - Range: 	  4500Km(3x200EFI), 2575km(1x200EFI), 2575Km(1X200EFI+2X640AP), 1600Km(2XRocket127) 
						  2575Km(1X200EFI+2X640AP+8XRocket127), 4500Km(3x200EFI+8XRocket127), 1570Km(1X2000AP)

#) AEROPORTO UPGRADE x Plane MaxWeight
	- Aeroporto Level 0 -> 7 Ton
	- Aeroporto Level 1 -> 15 Ton
	- Aeroporto Level 2 -> 30 Ton
	- Aeroporto Level 3 -> 200 Ton

==> Aparentemente é o <float name="MaxWeight" value="29.041"/> que limita o aeroporto dos avioes!
	===> Mas tambem limita o RANGE (pois ele nunca enxe de combustivel mais que o MaxWeight - Mesmo com DropTanks)!!


#) Para Aumentar o RANGE dos PLANES:
	===> Pode aumentar mais o FUEL, mas o MaxWeight limita quanto FUEL poe no aviao... Entao tem que aumentar MaxWeight tambem!!
	====> MaxWeight limita tambem os Fuel Tanks EFI...
	=====> Se aumentar muito, passar do limite dos Aeroportos, vai precisar de uma pista Maior (mais UPGRADE do Aeroporto)!


#) TRANSPORTAR AVIOES (F6F, SBD2, TBF): 
	- FAzer Aircraft Carrier Airgroup de 250 avioes (dando Joint em um monte de Corps)
	- A dica aqui e´ Esperar passar de 200 avioes antes de dar o Join for transportation em Transport Divisions de 4 transportes Victory (que eu modifiquei pra trnasportar 50 avioes!!), pois dai´ ele so´ esvasia o Corps, nao remove das formations e fica pronto pra enxer novamente com "E"


#) NAVIOS -> HISTORICO (no comeco da guerra): Procurar no SaveGame por: <object name="NumbersMap">

	-> USA -  9 battleships, 3 aircraft carriers, 12 heavy cruisers, 8 light cruisers, 50 destroyers, 33 submarines,

USA =>	<string name="0" value="B-17.48.Baltimor.14.Benson.120.Clivlend.14.Colorado.12.Essex.3.F2A2.100.F2A3.116.Gato.52.Lexington.3.Liberty.44.OS2U.108.P-40.48.PBY.104.SBD.216.Simarron.20.TBD.72."/>

----x----x---- 					
	-> JAPAs - 4 carriers, 9 battleships, 13 heavy cruisers, 7 light cruisers, and 35 destroyers.

JAPAs => <string name="1" value="A5M4.348.Akagi.8.B5N.384.D3A.248.F1M.452.Hokoku.66.I58.51.Kagero.208.Kazahaia.44.Ki-21.432.Mogami.11.Myoko.11.Nagato.10.Shimushu.40.Zuikaku.2."/>
					

