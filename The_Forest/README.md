# The Forest Guide

## 0) Type "developermodeon" tudo junto na tela inicial do jogo
- Testa com F1 ou F2 pra ver se funcionou

- Options, then to Gameplay, then toggle Allow Cheats to On (Tem que ta´ ligado)

- Usar Collor Grade = M131 pras CAVERNAS e ORIGINAL para o dia 

## 1) Mude o tempo de jogo para 0.5 --> gametimescale 0.3 	Change # to a number. Changes the speed of day/night cycle, hunger, energy, thirst etc. Much safer than time scale
gametimescale 5

## 2) Save  -> abre o menu de salvar em qq lugar

## 3) F3 e F2 mostram stats

## 4) goto hull -> Vai para o Aviao

## 5) unlimitedHairspray on    or     unlimitedHairspray off

## 6) cavelight [on/off] - Choose light or darkness in cave


## 6) additem 271  -> dynamite
additem 88 modern axe
additem 180 katana
additem 279 modern bow
additem 204 bone armor
additem 57 sticks
additem 138 climbing axe


## 7) Other Commands
checkday	Checks time of day, can be useful for multiplayer if joining
advanceday	Skips one day ahead, not much use
setcurrentday 100	Will change day to 100, any number can be used
timescale (number)	Changes the speed of the game, 0 pauses, 1 is normal, 2 is faster, etc. DON'T GO TOO HIGH, STAY BELOW 20 then work your way up
gametimescale #	Change # to a number. Changes the speed of day/night cycle, hunger, energy, thirst etc. Much safer than time scale
timescale 0	This will pause the game, press ESC twice to unpause
lightingtimeofdayoverride off	Turns off this command if it is turned on
lightingtimeofdayoverride HR:MM	Can be used to set anytime you wish, for example, 10:00 will make it 10 in the morning
lightingtimeofdayoverride morning	Sets game lighting to morning until turned off
lightingtimeofdayoverride noon	Sets game lighting to noon until turned off
lightingtimeofdayoverride sunset	Sets game lighting to sunrise until turned off
lightingtimeofdayoverride night	Sets game lighting to night until turned off
