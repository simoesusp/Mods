# Sons of the Forest

## MODs

### Red Manager
- https://github.com/ToniMacaroni/RedManager
  - Instala o RedLoader que carrega os MODs.
  - Usar o Proprio Red Manager para escolher e instalar os MODs.
  - Eu estou usando esse aqui: https://sotf-mods.com/mods/tonimacaroni/zippy

## Cheats

### 0) GOTO
- goto CaveAEntrance

### 1) Setgametimespeed

- setgametimespeed 0.3
  - Change speed -> 1=Normal; 0.5=Slow
- ------------


### 2) Spawnitem -> Add to Floor
- spawnitem 527 5
  - Batteries
- spawnitem 507 20
  - Crafted Arrow
- spawnitem 494 10
  - Bone Armor
- spawnitem 437 5
  - Meds
- spawnitem 626 1
  - Hang Glider
- spawnitem 362 80
  - Pistol Ammo
- spawnitem 363 80
  - Slug ammo
- spawnitem 523 10
  - Zipline Rope
- spawnitem 554 10
  - Tech Armor
- ------------
  - Crafted Spear: 474
  - Buckshot Ammo: 364
  - Carbon Fiber Arrow: 373
  - Crafted Arrow: 507
  - Grenade Ammo: 382
  - Molotov Ammo: 389
  - Pistol Ammo: 362
  - Slug ammo: 363
  - Stun Gun Ammo: 369
  - Zipline Rope: 523
- ------------
  - Gps Tracker: 412
- ------------
  - Bone Armor: 494
  - Creepy Armor: 593
  - Deer Hide Armor: 519
  - Golden Armor: 572
  - Gold Mask: 435
  - Leaf Armor: 473
  - Mutant Armor: 492
  - Red Mask: 391
  - Tech Armor: 554
- ------------
  - Duct tape: 419
  - Feather: 479
  - Rope: 403
  - Small Rock: 476
  - Stick: 392
  - Tarp: 504
  - Turtle Shell: 506
  - Wire: 418
  - Vodka Bottle: 414


## 3) Here is the full list of every item ID we currently know.

Air Tank – 469
Alcohol – 414
Aloe Vera – 451
Arm – 480
Arrowleaf – 454
Backpack – 402
Bacon Bite – 571
Batteries – 527
Blackberries – 595
Blueberries – 445
Bone – 405
Bone Armor – 494
Buckshot Ammo – 364
Buckshot Shotgun Shells – 363
C4 Brick – 420
Canned Foods – 434
Carbon Fibre Arrow – 373
Cash – 496
Chicory – 465
Circuit Board
Climbing Axe
Cloth – 415
Coins – 502
Crafted Bow – 443
Crafted Club – 477
Crafted Spear – 474
Creepy Armor – 593
Cross – 468
Crossbow – 365
Crossbow Bolt – 368
Devil's Club – 449
Duct Tape – 419
Electric Chainsaw – 394
Emergency Pack – 483
Energy Bar – 441
Energy Drink – 439
Energy Mix – 461
Energy Mix+ – 462
Feather – 479
Firefighter Axe
Fireweed – 453
Fish – 436
Flare – 440
Flashlight – 471
Fly Amanita – 400
Frag Grenade – 381
Golden Armor – 572
GPS Locator – 529
GPS Tracker – 412
Guarana Berries
Guest KeyCard – 526
Guide Book – 589
Health Mix – 455
Health Mix+ – 456
Hide Armor – 519
Horsetail – 450
King Oyster – 398
Laser Sight – 375
Leaf – 484
Leaf Armor – 473
Leg – 481
Lighter – 413
Log – 78
Meds – 437
Modern Axe – 356
Molotov – 388
MRE Pack – 438
Oyster – 466
Pistol – 355
Pistol Ammo – 362
Pistol Rail – 376
Pistol Slilencer – 374
3D Printed Arrow – 618
Printed Grappling Hook – 560
Printer Resin – 390
Printed Sled – 428
Radio – 590
Ramen Noodles – 421
Raw Meat – 433
Rebreather – 444
Repair Tool – 422
Revolver – 386
Rock – 393
Rope – 403
Rope Gun – 522
Salmonberries – 447
Shotgun – 358
Shotgun Rail – 346
Skin Pouch – 508
Skull – 430
Small Rocks – 476
Steak Bite – 570
Stick – 392
Stone Arrow – 507
Swimsuit – 619
Tactical Axe – 379
Tarp – 504
Taser – 353
Taser Cartridges – 369
Tech Armor – 554
Tech Mesh – 553
Torch – 503
Turtle Shell – 506
Twinberries – 446
Utility Knife – 380
Walkie Talkie – 486
Watch – 410
Weapon Flashlight – 499
Wetsuit – 499
Wood Coffin
Yarrow – 452
Zipline Rope – 523

