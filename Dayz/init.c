void main()
{
	//INIT ECONOMY--------------------------------------
	Hive ce = CreateHive();
	if ( ce )
		ce.InitOffline();

	//DATE RESET AFTER ECONOMY INIT-------------------------
	int year, month, day, hour, minute;
	int reset_month = 9, reset_day = 20;
	GetGame().GetWorld().GetDate(year, month, day, hour, minute);

	if ((month == reset_month) && (day < reset_day))
	{
		GetGame().GetWorld().SetDate(year, reset_month, reset_day, hour, minute);
	}
	else
	{
		if ((month == reset_month + 1) && (day > reset_day))
		{
			GetGame().GetWorld().SetDate(year, reset_month, reset_day, hour, minute);
		}
		else
		{
			if ((month < reset_month) || (month > reset_month + 1))
			{
				GetGame().GetWorld().SetDate(year, reset_month, reset_day, hour, minute);
			}
		}
	}
}

class CustomMission: MissionServer
{
	void SetRandomHealth(EntityAI itemEnt)
	{
		if ( itemEnt )
		{
			float rndHlt = Math.RandomFloat( 0.45, 0.65 );
			itemEnt.SetHealth01( "", "", rndHlt );
		}
	}

	override PlayerBase CreateCharacter(PlayerIdentity identity, vector pos, ParamsReadContext ctx, string characterName)
	{
		Entity playerEnt;
		playerEnt = GetGame().CreatePlayer( identity, characterName, "5301.78 0 10414.00", 0, "NONE" ); //*** playerEnt = GetGame().CreatePlayer( identity, characterName, pos, 0, "NONE" ); 
		Class.CastTo( m_player, playerEnt );

		GetGame().SelectPlayer( identity, m_player );

		return m_player;
	}

	override void StartingEquipSetup(PlayerBase player, bool clothesChosen)
	{
		EntityAI itemClothing;
		EntityAI itemEnt;
		ItemBase itemBs;
		float rand;

		/*
		itemClothing = player.FindAttachmentBySlotName( "Body" );
		if ( itemClothing )
		{
			SetRandomHealth( itemClothing );
			
			itemEnt = itemClothing.GetInventory().CreateInInventory( "BandageDressing" );
			player.SetQuickBarEntityShortcut(itemEnt, 2);
			
			string chemlightArray[] = { "Chemlight_White", "Chemlight_Yellow", "Chemlight_Green", "Chemlight_Red" };
			int rndIndex = Math.RandomInt( 0, 4 );
			itemEnt = itemClothing.GetInventory().CreateInInventory( chemlightArray[rndIndex] );
			SetRandomHealth( itemEnt );
			player.SetQuickBarEntityShortcut(itemEnt, 1);

			rand = Math.RandomFloatInclusive( 0.0, 1.0 );
			if ( rand < 0.35 )
				itemEnt = player.GetInventory().CreateInInventory( "Apple" );
			else if ( rand > 0.65 )
				itemEnt = player.GetInventory().CreateInInventory( "Pear" );
			else
				itemEnt = player.GetInventory().CreateInInventory( "Plum" );
			player.SetQuickBarEntityShortcut(itemEnt, 3);
			SetRandomHealth( itemEnt );
		}
		
		itemClothing = player.FindAttachmentBySlotName( "Legs" );
		if ( itemClothing )
			SetRandomHealth( itemClothing );
		
		itemClothing = player.FindAttachmentBySlotName( "Feet" );
		
		*/
		
		
		player.RemoveAllItems();		
		
		player.GetInventory().CreateInInventory( "Balaclava3Holes_Black" );
	    player.GetInventory().CreateInInventory( "M65Jacket_Black" );
	    player.GetInventory().CreateInInventory( "TacticalGloves_Black" );
	    player.GetInventory().CreateInInventory( "HunterPants_Autumn" );
	    player.GetInventory().CreateInInventory( "MilitaryBoots_Black" );
	    player.GetInventory().CreateInInventory( "AliceBag_Camo" );
	    player.GetInventory().CreateInInventory( "WoodAxe" );
	    player.GetInventory().CreateInInventory( "UMP45" );
		player.GetInventory().CreateInInventory( "HighCapacityVest_Black" );
		
	    player.GetInventory().CreateInInventory( "ACOGOptic_6x" );
		player.GetInventory().CreateInInventory( "PistolSuppressor" );
		
		player.GetInventory().CreateInInventory( "Mag_UMP_25Rnd" );
		player.GetInventory().CreateInInventory( "Mag_UMP_25Rnd" );
		player.GetInventory().CreateInInventory( "Mag_UMP_25Rnd" );
		player.GetInventory().CreateInInventory( "Mag_UMP_25Rnd" );

		
		
		// Novos Itens
		player.GetInventory().CreateInInventory( "Apple" );
		player.GetInventory().CreateInInventory( "AviatorGlasses" );
		player.GetInventory().CreateInInventory( "BakedBeansCan" );
		player.GetInventory().CreateInInventory( "BakedBeansCan" );
		player.GetInventory().CreateInInventory( "BloodBagEmpty" );
		//player.GetInventory().CreateInInventory( "BoneHook" );
		//player.GetInventory().CreateInInventory( "BoneKnife" );
		//player.GetInventory().CreateInInventory( "BurlapSack" );
		player.GetInventory().CreateInInventory( "CanOpener" );
		//player.GetInventory().CreateInInventory( "CharcoalTablets" );
		player.GetInventory().CreateInInventory( "CombatKnife" );
		player.GetInventory().CreateInInventory( "CombatKnife" );
		//player.GetInventory().CreateInInventory( "DeadRooster" );
		player.GetInventory().CreateInInventory( "DeadRooster" );
		player.GetInventory().CreateInInventory( "Deagle" );
		player.GetInventory().CreateInInventory( "Mag_Deagle_9rnd" );
		player.GetInventory().CreateInInventory( "PistolOptic" );
		player.GetInventory().CreateInInventory( "PistolSuppressor" );
		player.GetInventory().CreateInInventory( "IodineTincture" );
		player.GetInventory().CreateInInventory( "DuctTape" );
		player.GetInventory().CreateInInventory( "DuctTape" );
		//player.GetInventory().CreateInInventory( "Flashlight" );
		player.GetInventory().CreateInInventory( "Headtorch_Black" );
		player.GetInventory().CreateInInventory( "Lockpick" );

		//player.GetInventory().CreateInInventory( "Raincoat_Black" );
		player.GetInventory().CreateInInventory( "StartKitIV" );
		player.GetInventory().CreateInInventory( "BandageDressing" );
		player.GetInventory().CreateInInventory( "BandageDressing" );
		player.GetInventory().CreateInInventory( "HipPack_Black" );
		player.GetInventory().CreateInInventory( "BeanieHat_Green" );
		player.GetInventory().CreateInInventory( "ChestHolster" );
		//player.GetInventory().CreateInInventory( "Shovel" );
		player.GetInventory().CreateInInventory( "WaterBottle" );
		player.GetInventory().CreateInInventory( "WaterBottle" );
		//player.GetInventory().CreateInInventory( "BrassKnuckles_Dull" );
		player.GetInventory().CreateInInventory( "PortableGasStove" );
		player.GetInventory().CreateInInventory( "SmallGasCanister" );
		//player.GetInventory().CreateInInventory( "CrudeMachete" );
		player.GetInventory().CreateInInventory( "Compass" );
		player.GetInventory().CreateInInventory( "Battery9V" );
		player.GetInventory().CreateInInventory( "Battery9V" );
		player.GetInventory().CreateInInventory( "EpoxyPutty" );
		player.GetInventory().CreateInInventory( "SewingKit" );
		player.GetInventory().CreateInInventory( "WeaponCleaningKit" );
		player.GetInventory().CreateInInventory( "Whetstone" );
		player.GetInventory().CreateInInventory( "Canteen" );
		player.GetInventory().CreateInInventory( "Matchbox" );
		player.GetInventory().CreateInInventory( "WeaponCleaningKit" );
		player.GetInventory().CreateInInventory( "AmmoBox_357_20Rnd" );
		player.GetInventory().CreateInInventory( "AmmoBox_357_20Rnd" );
		player.GetInventory().CreateInInventory( "AmmoBox_357_20Rnd" );
		player.GetInventory().CreateInInventory( "AmmoBox_357_20Rnd" );
		player.GetInventory().CreateInInventory( "AmmoBox_45ACP_25rnd" );
		player.GetInventory().CreateInInventory( "AmmoBox_45ACP_25rnd" );
		player.GetInventory().CreateInInventory( "AmmoBox_45ACP_25rnd" );
		player.GetInventory().CreateInInventory( "AmmoBox_45ACP_25rnd" );
		player.GetInventory().CreateInInventory( "LeatherSewingKit" );
		player.GetInventory().CreateInInventory( "MediumTent" );
		//player.GetInventory().CreateInInventory( "SeaChest" ); 
	
	// -------x-------
// BandageDressing, Battery9V, Binoculars, Blowtorch, Canteen, ChickenBreastMeat, ChickenFeather, Compass, Crossbow_Black, EpoxyPutty, FirefighterAxe_Black, Firewood, FryingPan, GP5GasMask, GasMask_Filter, GasMask, HandSaw,  IodineTincture, LeatherBelt_Black, Matchbox, NailBox, PetrolLighter, Pot, SeaChest, SewingKit, Shovel, SkateHelmet_Black, SledgeHammer, SmallGasCanister, SmallStone, WaterBottle, WeaponCleaningKit, WoodenCrate, WoodenPlank, WoodenStick, WoodAxe, 

//WEAPONS - FAMAS, SVD, FNX45, 
	
		
		
		
		
	}
};

Mission CreateCustomMission(string path)
{
	return new CustomMission();
}