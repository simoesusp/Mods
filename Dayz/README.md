# Single player

- https://github.com/Arkensor/DayZCommunityOfflineMode

- BAixe o Zip das Missions
- Copie pra dentro do Folder do DayZ no Steam
- Va para o Steam e Click em Play DayZ
- VA em Parameters -> All Parameters  -> Missions  
- Selecione o Folder dentro do Missions que quer Jogar


## Controls
- Y (Z for QWERTZ-Keyboard) - Open the COM toolbar menu.
- X - Toggle Autojog/walk/run
- X + SHIFT - Enable Autorun (Just X to disable it again)
- X + CTRL - Enable Autowalk (Just X to disable it again)
- END - Teleport at the position you are looking at
- O - Spawn a random infected (Zombies)
- O + CTRL - Spawn a wolf (Agressive and will fight both players and infected)
- O + SHIFT - Spawn a random animal
- R - Reload the weapon and refill the ammo (Infinite ammo)
- P - Display your current position in the chat and print it to your logfiles (See logfiles section for their location)
- B - Toggle debug monitor
- INSERT - Toggle free camera. This teleports your player to the position you looked at when turning it off

## Object Editor Controls
- Click objects to select them.
- Click and drag objects to move them.
- Click on nothing to deselect the current object.
- Middle Click to snap to ground (Might not be accurate)
- Spawn in new items using the object spawner menu that can be found in the toolbar.
- You can either enter values on the onject editor gui or hover above the value with your mouse and use the scroll wheel to in-/decrease them.

## Loot and infected spawn
- By default the "Hive" that is repsonsible for spawning loot and infected is enabled. If you want to prevent loot and infected from spawning you need to follow this guide: Toggle loot and infected spawn

- Disabling the hive increases the game performace.

## Logfiles
- In case you want to report errors to us or the offical dayz dev team, you might need logfile info. We also save the positions you printed ingame in it so that you might revisit them later on by saving them in some textfile. Locations are stored inside the script.log for now.

- You find your logfiles here: Press WINDOWS + R  -> Type in %localappdata%/DayZ -> Hit enter.


# How you set up a Server for Single Player mode


- In your Steam library go to the Tools tab.
- Install DayZ Server.
- After it is installed right click it and select Properties then Launch Options, type this in there: -config=serverDZ.cfg

- Go to where the server files installed in steam and find \DayZServer\serverDZ.cfg
- Open it with notepad and change the name of the server to what ever you want, change day/night length etc... save the file.

- Just start the server from Steam like you would a game... wait for it to say init sequence finished then just start DayZ from Steam like you normaly would. When in the game select change server then go to the LAN tab and play on your private server that you named.

- If you want to save a backup of the server just copy the storage_1 folder somewhere else to recover from later.

- To reset the server delete all the files in the data folder: \DayZServer\mpmissions\dayzOffline.chernarusplus\storage_1\data\

- To delete/start a fresh character delete the players.db file under storage_1 folder:
\dayzOffline.chernarusplus\storage_1\players.db

- Later on if you want to play with mods on your sever you can right click DayZ game in your library and use the Dayz Launcher to add mods.

- For the new map you type: template="dayzOffline.enoch


