# Red Dead Redemption 2

## Missions:
- https://primewikis.com/red-dead-redemption-2/wiki-guide/

## Guns:
- Get Double Action Revolver at Lonnie's Shack near Rodes matando os 4 caras inside or...
  - Get Docs Schofield Revolver at Valentine
  - Beat and get Calloway Schofield Revolver at Valentine 2nd Saloon
- 1899 Pistol -> Buy in Chap 4
- Get Lancaster Repeater in Rodes no Porão do Gunsmith
- Get Rolling Block Rifle no Chap. 2 na missão de Rescue Sean
  - https://www.youtube.com/watch?v=5YAZ1JGoIIM

- Get Semi-Auto Shotgun at Whatson Cabin near Wallace Station

### Dead Eye
- Tier 2 - Manualy Target enemies
  - Hold SPACE to Auto-target like in Tier 1
- Unlock: Chapter 2 - Mission - Pouring Forth Oil IV

### Hip Fire or Aim Down Sight
- Hip Fire: Just Fire (LMB) dont aim (RMB)
  - Much faster and less precise
- Aim Down Sight: Aim first (RMB) and then Fire (LMB)

### -  Never grab WORN Guns cause you cannot throw them away!
- You won't be able to get Good (not Warn) ones!
- You can only have 2 revolvers of each type!
- If you already took one, just by a new one at the gunsmith


### -  Getting 2 Free Double Actions, Schofields and Volcanics in Chapter 2 
- https://www.youtube.com/watch?v=A4qRPFpLbOc


## Trinkets
- Buck trinket: Bether Pelts
- Fox trinket: increases the time that Eagle Eye can stay active by 5 seconds
- Coyote trinket: increases gained Dead Eye experience by 10%
- Ram trinket: Erbs 2x
- Beaver trinket: Weapon degradation
- Elk trinket: Money 2x
- Pronghorn (epilog) trinket: Pelts on horse dont spoil


### Do not sell this (you'll need for trinkets):
- Gold Joint Bracelet
- Gold Earring
- Silver Earring
- Silver Chain Bracelet

### -  PC Exclusive Trinkets:
- https://www.youtube.com/watch?v=IYIyr2NhSvI

  - Hawk Talon Trinket: Decreases Stamina drain by 30% when drawing a bow 
  - Cat Eye Trinket: Increases the duration of Fortifying tonic effects by 20% 
  - Shark Tooth Trinket: Increases horse bonding experience by 10% 
  - Turtle Shell Trinket (epilogue): Permanently increases health refill speed by 10%


## Saddles and Stirrups
- Gerden Vaquero Saddle: Stamina Drain -16% - Core Drain -14% - Stamina Regen +16%
- Hooded Stirrup: +2 Speed, +2 Acceleration, +50% stamina Drain Rate
- Panther Saddle: Stamina Drain -26% - Core Drain -20% - Stamina Regen +24%, +2 Speed, +2 Acceleration, +50% stamina Drain Rate
- RattleSnake Saddle: Stamina Drain -24% - Core Drain -22% - Stamina Regen +24%, +2 Speed, +2 Acceleration, +50% stamina Drain Rate
  - Need 10 Perfect Snake Pelts (Small game arrow)


## Horses
- White Arabian - chap 2 - Domar na Neve
  - Speed 6, acc 5
- Thoroughbred - chap3 - Strawberry Stable
  - Speed 7, acc 5
- Turkoman - chap4 - Saint Denis Stable or chap 3 - mission deliver horses
  - Speed 6, acc 5
- Black Arabian - chap 4 - Saint Denis Stable or chap 3 - roubar as 22h
  - Speed 6, acc 5
- Missouri Fox Trotter - Epilog - Black water Stable
  - Speed 7, acc 5

### -  Different horse tricks
- Dodge on horseback: hold RIGHT CLICK to aim, then press SPACE to perform dodge movement on horse.

- Horse rearing: hold left CTRL, then press SPACE bar.

- Horse walk in place: Hold SPACE while stationary. Then hold LEFT CLICK.

- Horse strafing: Hold SPACE + A/D

### -  Lasso a wild Horse from My Horse and dismount ("toggle aim"):
- Equip lasso > press and hold the RMB to aim
- Next press and hold LMB to activate and spin
- Release LMB to throw
- Then PRESS AND HOLD LMB AGAIN to DISMOUNT and keep the target lassoed
- Press E to dismount
- Click RMB again, and then release LMB -> the target should remain lassoed without having to hold down anything
- Walk to the target
- Press G to calm down horse
- Mount
- Press D a cada pinote do cavalo
- Press G to calm down the horse

  - Every time I get off the horse he lets go of the lasso; it's really frustrating. I switched to "hold to aim" instead of "toggle aim" and it works. If I hold RMB while getting off the horse the lasso is held. However using toggle aim it seems it's not possible to hold the lasso.
  - Just found out that it is possible to keep the target lassoed when using toggle aim - you must continue to hold the LMB when using toggle aim (the same button you use to fire your gun or pull the target toward you with the lasso). After getting of your horse you can click aim again (RMB) and then release LMB at this point and the target should remain lassoed without having to hold down anything.

## Shashel Upgrades
- Do first mission in Chap 2
- Buy Leather working tools no Ledger do Camp
- You will nedd: 
  - 7 Dear
  - 2 Elk
  - 1 Buck
  - 1 Badger
  - 1 Squirrel
  - 1 Panther
  - 1 Bison
  - 1 Raccoon
  - 1 Boar
  - 1 Iguana
  - 1 Beaver
  - 1 Rabbit
  - 1 Cougar
  - 1 Wolf


  ## Where to find Perfect Pelts


- Deer Perfect Pelts:
All seven of the satchel upgrades require Perfect Deer Pelts in order to craft upgrades. Luckily, deer of all types can be found in most areas of the game (except New Austin), and are found in heavy concentrations in Roanoke Ridge of New Hanover, and the Scarlet Meadows of Lemoyne.


  - Be sure to use long distance rifles for clean headshots, or bows - preferably with poison arrows to get a perfect pelt.


- Elk Perfect Pelts:
Two elk perfect pelts are needed for satchel upgrades, and unlike deer, elk are located in more selected habitats. Explore the Big Valley and Little Creek River of West Elizabeth, the Dakota River that runs through New Hanover, and the Cumberland Forest, as well as the Annesburg area in the north of Roanoke Ridge to have a good chance of coming across some elk.


  - Like Deer, use your long distance rifles or stick with bows for a clean kill.


- Badger Perfect Pelt:
To get a Perfect Badger Pelt, you will need to search around in either West Elizabeth or Lemyone for the densest badger population. Look for them in the Tall Trees of West Elizabeth, or all along the Scarlett Meadows of Lemoyne. You can even find many of them on the east side of Donner Falls up by East Grizzles in Ambarino.


  - With their moderate size, you must use a Varmint Rifle if you wish to score a Perfect Pelt from them.




- Squirrel Perfect Pelt:
Squirrels can be found almost anywhere in the world of Red Dead, excluding places like the far north of Ambarino, Blackwater, the swamps surrounding Saint Denis, or the plains of The Heartlands.


  - Remember that due to their small size, the only way to obtain Perfect Squirrel Pelts is by using a bow with Small Game Arrows.


- Panther Perfect Pelt:
The panthers of Red Dead 2 live exclusively in two locations: the marshy fields of Bolger Glade, and on the east edge of the lake where the town of Lagras stands.


  - With their large size, its best to use rifles from long range, or poison arrows to get the job done for a perfect pelt.


- Bison Perfect Pelt:
Bison can be found in few key locations around the game, which can include the Great Plains just outside of Blackwater, near the Heartland Overflow in New Hanover, and sometimes to the East of Flatneck Station.


  - Bison are huge animals, and require either precise strikes with improved arrows from bows, or specialty ammo from Sniper Rifles to make a clean kill.


- Raccoon Perfect Pelt:
Raccoons, much like squirrels, can be found all over the place in the wilderness so long as it's away from extreme areas like the cold north, the great empty plains of New Hanover, or the bayous of Lemoyne. They are found in greater numbers along the Dakota River, and in the far western reaches of West Elizabeth.


  - Given their moderate sizes, Varmint Rifles are a must if you wish to claim a perfect pelt from a clean kill.




- Boar Perfect Hide:
Wild Boars, not to be confused with pigs, can be found in great numbers along the Bluewater Marsh of Lemyone, and a bit north along the Kamassa River leading up to New Hanover. They can also be found in the southwestern areas of Lemoyne around the area of Rhodes.


  - When hunting boars, be sure to use rifles with high powered ammo to take the hearty animals down with a clean shot.


- Iguana Perfect Skin:
Iguanas are rare lizards that can only be found in select regions - like New Austin. However, they can also be found on the island chain on the east side of Flat Iron Lake near Rhodes in Lemoyne, and in Guarma when you travel there for a specific chapter.


  - Iguanas are around the same size as moderate animals, and so will need a Varmint Rifle to safely kill and get a perfect skin from them.


- Beaver Perfect Pelt:
Beavers are animals with a very specific habitat, and only congregate near particular bodies of water in Red Dead 2: You can find them on the north end of Lake Isabella up in the West Grizzlies of Ambarino. However, they are found in greatest numbers on the south side of the Lake Owanjila next to the town of Strawberry in West Elizabeth, but have also been spotted south along the Montana River as well.


  - Beavers are one of the few medium-sized animals, making them perfect to take out with a well placed bow shot or repeater to the head.


- Rabbit Perfect Pelt:
Rabbits can be found all over the land, though they rarely venture far north or deep into New Austin, they can be found almost anywhere else in high abundance.


  - With their small size, they are best taken out with Small Game Arrows to get a Perfect Pelt.




- Cougar Perfect Pelt:
Cougars are located in very specific hiding spots and generally avoid Lemoyne, Ambarino, and most of New Hanover. However, they can be found around the river in Roanoke Valley in northeastern New Hanover. They are more often found hunting in Little Creek River in West Elizabeth, or on the north or south sides of Tall Trees, as well as south of Blackwater. They've also been known to haunt locations in New Austin like around Benedict Point, and around Cholla Springs.


  - Due to their large size, prioritize rifles or a bow with poison arrows to take them down for a perfect pelt.


- Wolf Perfect Pelt:
Wolves can be located in the wilder parts of the maps - sometimes appearing to ambush you in the West Grizzles where the game's prologue took place. As well as areas above the Little Creek River in West Elizabeth, or in Grizzlies East, north of O'Creagh's Run Lake in Ambarino.


  - With their large size and predatory tactics, you'll need to be quick with a rifle to score a headshot using Deadeye, or employ arrows, such as poison arrows to get a perfect pelt.
