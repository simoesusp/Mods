# Cheats

## FRASE	DESCRIÇÃO	PRÉ-REQUISITO

- **Abundance is the dullest desire**	Munição infinita	Compre o jornal New Hannover Gazette No 27 em Valentine durante o Capítulo 1
- **Greed is american virtue**	Adiciona armas pesadas ao seu inventário	Compre o jornal após completar a missão “Propaganda, a Nova Arte Americana” no Capítulo 3
- **The lucky be strong evermore**	Fôlego infinito	Compre um jornal após completar o Capítulo 5
- **Virtue unearned is not virtue**	Aumenta o seu nível de Honra	Compre o jornal após completar a missão “Prazeres Urbanos” no Capítulo 4
- **Would you be happier as a clown?**	Faz aparecer uma carroça de circo	Compre o jornal após completar o Epílogo
- **You are a beast built for war**	Faz aparecer um cavalo de guerra	Compre o jornal após completar o Epílogo
- **You long for sight but See nothing**	Revela o mapa por completo	Compre o jornal após completar a missão “Rixas de Família, Velhas e Novas” no Capítulo 3
- **You seek more than the world offers**	Restaura e fortalece as barras de fôlego, de vida e dos Olhos da Morte	Compre o jornal após completar a missão “O Filho do Rei” no Capítulo 6
- **Greed is now a virtue**	Adiciona $500 à sua carteira	Nenhum
- **You flourish before you die**	Restaura as barras de vida, de fôlego e dos Olhos da Morte	Nenhum
- **Make me better**	Define no nível 2 a habilidade Olhos da Morte (permite marcar manualmente os inimigos)	Nenhum
- **I shall be better**	Define no nível 3 a habilidade Olhos da Morte (mantém a habilidade ativa após atirar)	Nenhum
- **I still seek more**	Define no nível 4 a habilidade Olhos da Morte (destaca áreas fatais ao mirar no inimigo)	Nenhum
- **I seek and I find**	Define no nível 5 a habilidade Olhos da Morte (destaca áreas fatais e de dano crítico ao mirar no inimigo)	Nenhum
- **Keep your dreams simple**	    Faz surgir uma carroça com um cavalo	Nenhum
- **Keep your dreams light**	    Faz surgir um cavalo com um carreto	Nenhum
- **My kingdom is a horse**	        Aumenta o vínculo com todos os seus cavalos	Nenhum
- **Run! Run! Run!**	            Faz aparecer um cavalo de corrida	Nenhum
- **You want something new**	    Faz aparecer um cavalo aleatório	Nenhum
- **You want more than you have**	Faz aparecer um cavalo Puro-Sangue Árabe	Nenhum
- **Better than my dog**	        Permite chamar o seu cavalo de qualquer distância	Nenhum
- **Vanity. All is vanity**	        Desbloqueia todas as roupas no guarda-roupa de Arthur	Nenhum
- **Balance. All is balance**	    Restaura a barra de Honra para a posição neutra	Nenhum
- **A fool on command**	            Deixa o personagem completamente bêbado	Nenhum
- **Death is silence**	            Adiciona armas silenciosas ao seu inventário	Nenhum
- **History is written by fools**	Adiciona armas de pistoleiro ao seu inventário	Nenhum
- **A simple life, a beautiful death**	Adiciona armas simples ao seu inventário	Nenhum
- **You revel in your disgrace, I see**	Define a Honra no nível mínimo	Nenhum
- **Eat of knowledge**	            Desbloqueia todas as receitas de criações	Nenhum
- **Share**	                        Desbloqueia todas as melhorias do livro de registro do acampamento e aumenta ao nível máximo os suprimentos	Nenhum
- **You want freedom**	            Diminui o seu nível de procurado	Nenhum
- **You want punishment**	        Aumenta o seu nível de procurado	Nenhum
- **Seek all the bounty** of this place	Aumenta as barras dos atributos	Nenhum


